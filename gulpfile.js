const { src, dest } = require('gulp');
var mainNpmFiles = require('gulp-main-npm-files');
var prepareLibs = require('./prepare-libs');
const terser = require('gulp-terser');

function libs() {
    return src(mainNpmFiles())
        .pipe(prepareLibs())
        .pipe(terser())
        .pipe(dest('public'));
}

function defaultTask(cb) {
    return libs();
}

  exports.default = defaultTask;